# encoding: utf-8

import logging
import sys


datefmt = '%Y-%m-%dT%H:%M:%S%z'
fmt = '%(asctime)s %(levelname)s %(message)s'


def get_logger(name, level=logging.INFO):
  if level not in ('CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG'):
    level = logging.INFO

  handler = logging.StreamHandler(stream=sys.stdout)
  handler.setFormatter(logging.Formatter(datefmt=datefmt, fmt=fmt))

  log = logging.getLogger(name)
  log.addHandler(handler)
  log.setLevel(level)

  return log


class SomeError(Exception):
  pass
