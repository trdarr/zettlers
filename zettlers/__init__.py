# encoding: utf-8

import functools
import logging
import operator
import sys
from urllib.parse import urljoin

import flask
import requests
import werkzeug.contrib.fixers
from flask import Response
from flask import flash, redirect, render_template, request, url_for

from zettlers.util import SomeError


config = {'api_base_uri': 'http://localhost:5002/'}


# If there’s no user id in the request context, redirect to the login view.
def login_required(function):
  @functools.wraps(function)
  def decorated_function(*args, **kwargs):
    try:
      flask.session['token']
    except KeyError:
      flask.current_app.logger.info('No session token in request.')
      flash('You must be logged in.')
      return redirect(url_for('login_view'))
    return function(*args, **kwargs)
  return decorated_function


# Create the Flask application.
app = flask.Flask(__name__)
app.secret_key = 'lol'  # lol

@app.before_first_request
def set_logger():
  if not app.debug:
    app.logger.addHandler(logging.StreamHandler(sys.stdout))
    app.logger.setLevel(logging.INFO)


@app.route('/')
@login_required
def index_view():
  uri = urljoin(config['api_base_uri'], '/sessions')
  response = requests.get(uri, params={'token': flask.session['token']})
  response_json = response.json()
  email = response_json['email']
  sessions = response_json['sessions']
  return render_template('sessions.html.j2', title='Recent activity',
      email=email, sessions=sessions)


@app.route('/login', methods=('GET', 'POST'))
def login_view():
  if request.method == 'GET':
    return render_template('login.html.j2', title='Log in')  # Logga in')

  try:
    email, password = required_fields(request.form, 'email', 'password')
  except SomeError as e:
    app.logger.warning(str(e))
    flask.flash(str(e))
    return redirect(url_for('login_view'),
        email=request.form.get('email', None))

  uri = urljoin(config['api_base_uri'], '/login')
  response = requests.post(uri, json={'email': email, 'password': password})
  if response.status_code != 200:
    flash(response.json()['error'])
    return redirect(url_for('login_view'))

  flask.session['token'] = response.json()['token']
  return redirect(url_for('index_view'))


@app.route('/logout')
def logout_view():
  flask.session.clear()
  return redirect(url_for('login_view'))


@app.route('/register', methods=('GET', 'POST'))
def register_view():
  if request.method == 'GET':
    return render_template('register.html.j2', title='Register')

  try:
    email, password = required_fields(request.form, 'email', 'password')
  except SomeError as e:
    app.logger.warning(str(e))
    flask.flash(str(e))
    return redirect(url_for('register_view'),
        email=request.form.get('email', None))

  uri = urljoin(config['api_base_uri'], '/register')
  response = requests.post(uri, json={'email': email, 'password': password})
  if response.status_code != 200:
    flash(response.json()['error'])
    return redirect(url_for('register_view'))

  flask.session['token'] = response.json()['token']
  return redirect(url_for('index_view'))


def required_fields(body, *fields):
  try:
    return operator.itemgetter(*fields)(body)
  except KeyError:
    message = 'Missing required field(s): {}.'
    missing_fields = filter(lambda f: f not in body, fields)
    raise SomeError(message.format(', '.join(missing_fields)))
