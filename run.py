#!/usr/bin/env python3
# encoding: utf-8

from zettlers import app
app.run('0.0.0.0', port=5001, debug=True)
