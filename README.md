This depends on Python 3. On OS X, you can install Python 3 with Homebrew.

```bash
brew install python3
pip install virtualenv
```

Create a virtual environment, install the dependencies, and run the thing.

```bash
virtualenv -p python3 .venv
source .venv/bin/activate
python -m zettlers
```

You should have an application running on http://localhost:5001/.
It depends on the corresponding API application running on 5002.
